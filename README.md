# Password generator
Generator for easy to read and type passwords. They will look like this<br/>
``ldxe 1494 WUZZ OGOH NMRN XIIX LOOE``

## Usage
``./generate_password [word [wordsize [delimiter [suffix]]]]``
- words ...... number of word (min = 3, default = 7)
- wordsize ... number of characters within one word (min = 3, default = 4)
- delimiter ... The delimiter between two words. Enhances readability (default = space)
- suffix ...... String appended to the password (default = '')

## Details

A generated password consists of a number of words.<br/>
A word consists of lowercase or uppercase or digits - they are not mixed within one word.<br/>
The words are delimited with a delimiter character.<br/>
The sequence is always
- Some lowercase words
- One digits word
- Some uppercase words
- Optional suffix
The exact total number of words can be defined (cmdline arg #1), default = 7<br/>
The size of each word can be defined (cmdline arg #2), default = 4<br/>
The delimiter between words can be defined (cmdline arg #3), default = ' ' (space)<br/>
The digits-word is generated at a random position, somewhere between position 2 and wordcount-1<br/>
An optional suffix can be appended, e.g. in order to add an enforced special character.

## Password strength
Due to predictable character class (lower, upper, digit), whitespaces and limited character set the passwords can only be made safe by making it long enough. We assume that on a mobile phone typing 34 characters of the same lower/upper/digit class is still faster and less error prone than typing 10 pure random characters with special characters.
No calculations concerning entropy were made to proof this. Probably 128-160 bits (16-20 bytes)

## Examples
``ldxe 1494 WUZZ OGOH NMRN XIIX LOOE`` Digits in 2nd word<br/>
``ldxe wuzz oqoh nmrn xiix 1494 LOOE`` Digits in 2nd to last word<br/>
``ldxe wuzz oqoh 1494 NMRN XIIX LOOE`` Digits in the middle<br/>

This will not happen:

``1494 WUZZ OGOH NMRN XIIX LOOE LDXE`` Digits in 1st word<br/>
``ldxe wuzz oqoh nmrn xiix looe 1494`` Digits in last word<br/>

## Requirements
1. Easy to read (e.g. from printout or screen)
2. Easy to type, especially on mobile phone keyboards
3. Adapt to over-ambitious password requirements
   1. No spaces allowed (arrgh)
   2. Enforced special character
   3. Limited max. length

### (Non-requirements)
- Easy to remember

## How requirements are achieved
1. *Easy to read:* Characters are grouped to "words" which are seperated by space.
2. *Easy to type:* Each "word" consists of only lowercase, only uppercase or only digits. All lowercase words and uppercase words are consecutive. Thus the keyboard mode on mobile has to be changed max. 3 times instead of once per character for truly random passwords. Because of the seperated words you can read and remember one word, then type it. Then it's easy to find the next word to read.
3. &nbsp;
    1. *No spaces allowed:* Change delimiter character to empty ('') or some other character, e.g. '\_'<br/>
    ``./generate_password.sh 7 4 ''``
    2. *Enforced special characters:* Use special character as suffix or delimiter<br/>
    ``./generate_password.sh 7 4 '$'``<br/>
    ``./generate_password.sh 7 4 ' ' '$'``
    3. *Limited max. length:* Reduce word count or size. The min. password length is 9 characters<br/> ``./generate_password.sh 3 3 ''``.
