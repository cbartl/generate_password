#defaults
declare -i defaultgroups=7
declare -i defaultgroupsize=4
defaultdelimiter=' '
defaultsuffix=''

#initial values
declare -i groups=0
declare -i groupsize=0
delimiter='undef'
suffix='undef'

# cmd arguments
[[ $# -ge 1 ]] && groups=$1
[[ $# -ge 2 ]] && groupsize=$2
[[ $# -ge 3 ]] && delimiter=$3
[[ $# -ge 4 ]] && suffix=$4

# merge cmd arguments and defaults
[[ $groups -eq 0 ]]           && groups=$defaultgroups
[[ $groupsize -eq 0 ]]        && groupsize=$defaultgroupsize
[[ "$delimiter" =  'undef' ]] && delimiter="$defaultdelimiter"
[[ "$suffix" =  'undef' ]]    && suffix="$defaultsuffix"

# input validation
if [[ $groups -lt 3 ]]; then
    echo "Arg #1 error: Min. 3 groups required"
    exit 1
fi
if [[ $groupsize -lt 3 ]]; then
    echo "Arg #2 error: Min. groupsize = 3"
    exit 1
fi

posdigit=$(($RANDOM % ($groups-2) + 2))
pw=""

# lowercase words
countlower=$((posdigit - 1))
while [[ $countlower -gt 0 ]]; do
    pw+=$(cat /dev/urandom | tr -dc 'a-z' | fold -w $groupsize | head -n 1)
    pw+=$delimiter
    ((countlower--))
done

# digits
pw+=$(cat /dev/urandom | tr -dc '0-9' | fold -w $groupsize | head -n 1)
pw+=$delimiter

# uppercase words
countupper=$((groups - posdigit))
while [[ $countupper -gt 0 ]]; do
    pw+=$(cat /dev/urandom | tr -dc 'A-Z' | fold -w $groupsize | head -n 1)
    pw+=$delimiter
    ((countupper--))
done

# trim pw to real length and append suffix
declare -i len
len=${#delimiter}
((len+=$groupsize))
((len*=$groups))
((len-=${#delimiter}))
pw=${pw:0:$len}
pw+=$suffix

###############
echo "$pw"
###############
